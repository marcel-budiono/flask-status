activate_this = '/var/www/flask/flask-status/env/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

import sys
sys.path.insert(0, '/var/www/flask/flask-status')

from FlaskStatus import app as application
from FlaskStatus import config

config.load_config()