from flask import render_template
from FlaskStatus import app

@app.route('/')
@app.route('/home/')
def home():
	"""Renders home page"""
	return render_template(
		'home.html',
		title='Home'
	)
