
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto import Random
from binascii import hexlify, unhexlify
import json
import sys

config = {}
config_file_name = 'config.json'

def make_key(key):
	"""Make 256-bit hashed key"""
	h = SHA256.new(data=key)
	return h.digest()

def generate_iv():
	global config
	iv = Random.new().read(AES.block_size)
	config['iv'] = hexlify(iv)	
	return iv

def make_cipher():
	global config
	key = make_key(b'qsoft-program-trading')
	iv = unhexlify(config['iv'])
	return AES.new(key, AES.MODE_CFB, iv)

def save_config():
	global config
	global config_file_name
	if not config or not isinstance(config, dict):
		config = {}

	config.setdefault('user', 'root')
	config.setdefault('password', 'secret')
	config.setdefault('server', 'localhost')
	config.setdefault('port', 3306)
	config.setdefault('database', 'logdb')

	if 'iv' in config:
		# if there is an iv then secret data is encoded
		secret_fields = ['user', 'password']
		secret = {}
		# mode secret fields to secret
		for secret_field in secret_fields:
			secret[secret_field] = config[secret_field]
			del config[secret_field]

		# encode secret data
		config['cipher'] = hexlify(make_cipher().encrypt(json.dumps(secret)))

	fp = open(config_file_name, 'w')
	json.dump(config, fp, indent=2, separators=(',', ': '))
	fp.close()

def load_config():
	global config
	try:
		config = json.load(open(config_file_name))
	except:
		print "config file : %s not found" % (config_file_name)
		print "recreate config file using: %s --generate-config" % (sys.argv[0])
		sys.exit(1)

	if 'iv' in config:
		secret = json.loads(make_cipher().decrypt(unhexlify(config['cipher'])))
		for (key, value) in secret.iteritems():
			if key not in config:
				config[key] = value

	config.setdefault('user', 'root')
	config.setdefault('password', 'secret')
	config.setdefault('server', 'localhost')
	config.setdefault('port', 3306)
	config.setdefault('database', 'logdb')

def switch():
	if len(sys.argv) > 1:
		# argument(s) supplied
		cmds = sys.argv[1:]
		while len(cmds) > 0:
			# parsing thru arguments
			cmd = cmds.pop(0).lower()
			if cmd == '--generate-config':
				pass
			elif cmd == '--generate-iv':
				generate_iv()
			elif cmd == '--set-user':
				user = cmds.pop(0)
				config['user'] = user
				pass
			elif cmd == '--set-password':
				password = cmds.pop(0)
				config['password'] = password
			else:
				print "Unknown command :", cmd
		save_config()
		return False
	else:
		load_config()
		return True

if __name__ == '__main__':
	switch()