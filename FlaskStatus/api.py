from flask import jsonify
from FlaskStatus import app, config
import MySQLdb
import sensors

@app.route('/slave-status/')
def slaveStatus():
    try:
        db = MySQLdb.connect(
            host = config.config['server'],
            port = config.config['port'],
            db = config.config['database'],
            user = config.config['user'],
            passwd = config.config['password'])

        cur = db.cursor(MySQLdb.cursors.DictCursor)
        cur.execute('SHOW SLAVE STATUS')
        status = cur.fetchone()
        db.close()
        return jsonify(
            success=True,
            data=status,
            server=config.config['server'],
            port=config.config['port'],
            database=config.config['database'])
    except:
        return jsonify(
            success=False,
            data=None,
            server=config.config['server'],
            port=config.config['port'],
            database=config.config['database'])

@app.route('/sensors/')
def sensorStatus():
    return jsonify(success=True, data=readSensors())

def readSensors():
    sensors.init()
    try:
        data = list({
            'chip': '%s' % chip,
            'adapter': chip.adapter_name, 
            'features': list({
                    'label': feature.label,
                    'value': feature.get_value(),
                    'name': feature.name,
                    'number': feature.number,
                    'type': feature.type
                } for feature in chip)
        } for chip in sensors.iter_detected_chips())
    finally:
        sensors.cleanup()
    return data
