'use strict';

(function(angular) {
    var homeApp = angular.module('app', []);

    homeApp.controller('SlaveStatusCtrl', function ($scope, $http, $timeout) {
        $scope.data = $scope.data || {};
        var refresh = function() {
            $http.get('/slave-status')
                .success(function (resp) {                
                    if (resp.success) {
						$scope.data = {
							'Slave I/O Running': resp.data['Slave_IO_Running'],
							'Slave I/O State': resp.data['Slave_IO_State'],
							'Seconds Behind Master': resp.data['Seconds_Behind_Master'],
							'Read Master Log Pos': resp.data['Read_Master_Log_Pos'],
							'Exec Master Log Pos': resp.data['Exec_Master_Log_Pos'],
							'Last I/O Error': resp.data['Last_IO_Error'],
							'Last SQL Error': resp.data['Last_SQL_Error'],
						    'Slave server': resp['database'] 
							    + '@' + resp['server'] 
								+ ':' + resp['port'],
						};
					} else {
					    $scope.data = {
						    'Slave server': resp['database'] 
							    + '@' + resp['server'] 
								+ ':' + resp['port'],
					    };
					}
                    $timeout(refresh, 5000);
                });
        };
        refresh();
    });

    homeApp.controller('SensorStatusCtrl', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {
        $scope.data = $scope.data || [];
        var refresh = function() {
            $http.get('/sensors')
                .success(function (resp) {
                    $scope.data = resp.data;
                    $timeout(refresh, 5000);
                });
        };
        refresh();
    }]);
})(angular);
