from FlaskStatus import app, config

if __name__ == '__main__':
    if config.switch():
        app.run(host='0.0.0.0', debug=True)
